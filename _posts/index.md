---
{"title": "home", "sequence": 0}
---

## Who am I?

I am a Python and Javascript developer working for [Odoo](https://odoo.com), and part-time freelance developer.

## Find Me
 - [GitHub](https://github.com/kba-odoo)
 - [LinkedIn](https://linkedin.com/in/kevin-baptiste/)
 - [Email](mailto:coucou at s.baptiste.cc)